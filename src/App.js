import './App.css';
import { client } from './apolloClient';
import { ApolloProvider } from '@apollo/client';
import Blog from './Pages/blog/Blog'
import Post from './Pages/blog/post'
import Products from './Pages/shop/products'
import Product from './Pages/shop/single-product/product'
import { BrowserRouter, Route } from 'react-router-dom'
import Header from './Components/Header';

function App() {

  return (<ApolloProvider client={client}>
    <div className="App">
      <BrowserRouter>
      <Header />
        <Route exact path="/blog" component = {Blog}/>
        <Route exact path="/post/:id" component = {Post}/>

        <Route exact path="/shop" component = {Products}/>
        <Route exact path="/shop/:slug" component = {Product}/>
        
      </BrowserRouter>

    </div>
  </ApolloProvider>);
}

export default App;
