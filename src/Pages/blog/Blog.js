//import './App.css';
import {BLOG_QUERY} from './blogQuery';
import { useQuery } from '@apollo/client';
import { Link } from 'react-router-dom'


function Blog() {
    const { loading, data } = useQuery(BLOG_QUERY);

    if(loading){
        return 'loading';
    }

    let posts = data.posts.nodes;
    

    
  return (
          <div className="blog-wrap container">
              <div className='row'>
                {posts.map(post => {
                    return <Link to={`post/${post.postId}`} key={post.id} className='col-md-4'>
                        <div    className='featureimg' 
                                style={{ backgroundImage:post.featuredImage !== null ? `url(${post.featuredImage.node.sourceUrl})`: undefined }}>
                             
                        </div>
                         
                        <h2  dangerouslySetInnerHTML={{__html : post.title}}></h2>
                        <div  dangerouslySetInnerHTML={{__html : post.excerpt}}></div>
                    </Link>
                })}
              </div>
          </div>
    );
}

export default Blog;
