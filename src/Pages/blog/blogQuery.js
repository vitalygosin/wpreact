import { gql } from '@apollo/client';

export const BLOG_QUERY = gql`
query MyQuery {
    posts {
        nodes {
          excerpt
          id
          title
          featuredImage {
            node {
              sourceUrl
            }
          }
          slug
          postId
        }
      }
  }    
`;