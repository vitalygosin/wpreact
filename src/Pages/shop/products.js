import {SHOP_QUERY} from './shopQuery';
import { useQuery } from '@apollo/client';
import { Link } from 'react-router-dom'


function Products() {
    const { loading, data } = useQuery(SHOP_QUERY);

    if(loading){
        return 'loading';
    }
   console.log(data);
    let products = data.products.nodes;
    
  return (
          <div className="blog-wrap container">
              <div className='row'>
                {products.map(product => {
                    return <Link to={`shop/${product.slug}`} key={product.id} className='col-md-4'>
                        <div    className='featureimg' 
                                style={{ backgroundImage:product.image !== null ? `url(${product.image.sourceUrl})`: undefined }}>
                             
                        </div>
                         
                        <h2  dangerouslySetInnerHTML={{__html : product.name}}></h2>
                        <h2  dangerouslySetInnerHTML={{__html : product.price}}></h2>
                        <div  dangerouslySetInnerHTML={{__html : product.description}}></div>
                    </Link>
                })}
              </div>
          </div>
    );
}

export default Products;
