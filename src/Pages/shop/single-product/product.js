//import './App.css';
import {PRODUCT_QUERY} from './productQuery';
import { client, useQuery } from '@apollo/client';
import { Link } from 'react-router-dom'
//import Price from "./single-product/price";
//import AddToCartButton from "./single-product/addToCartBtn";


function Product(props) {
    
  let slug = props.match.params.slug;
  
  console.log(slug);
    const { loading, data } = useQuery(PRODUCT_QUERY, {
        variables: { slug },
      });

    if(loading){
        return 'loading';
    }
    
    let product = data.product;
    

    
  return (
          <div className="product-wrap container">
              <div className='row'>
                <div key={product.id} className='col-md-8'>
                
                        <div    className='featureimg' 
                                style={{ backgroundImage:product.image ? `url(${product.image.sourceUrl})`: undefined }}>  
                        </div>
                        <h2  >{product.name}</h2>
                        <h2  >{product.price}</h2>
                        <div  dangerouslySetInnerHTML={{__html : product.description}}></div>
                        {/* <Price salesPrice={product?.price} regularPrice={product?.regularPrice}/> */}
					              {/* <AddToCartButton product={ product }/> */}
                        <Link to={`/shop`}><button>back to shop</button></Link>
                    </div>
              </div>
          </div>
    );
}

export default Product;
