import { gql } from '@apollo/client';

export const SHOP_QUERY = gql`
query MyQuery {
      products(first: 50) {
        nodes {
          id
          averageRating
          slug
          description
          image {
              id
              uri
              title
              srcSet
              sourceUrl
            }
          name
          ... on SimpleProduct {
              price
              regularPrice
              id
            }
          ... on VariableProduct {
              price
              id
              regularPrice
            }
          ... on ExternalProduct {
              price
              id
              externalUrl
              regularPrice
            }
          ... on GroupProduct {
            id
            products {
                nodes {
                ... on SimpleProduct {
                  id
                  price
                  regularPrice
                }
              }
            }
          }
        }
      }
  }    
`;