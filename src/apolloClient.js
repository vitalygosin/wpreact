import { ApolloClient, InMemoryCache } from '@apollo/client';
import { gql } from '@apollo/client';

export const client = new ApolloClient({
  uri: 'https://goodwpress.com/graphql',
  cache: new InMemoryCache()
});

