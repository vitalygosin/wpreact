import { gql } from '@apollo/client';

// export const POST_QUERY = gql`
// query MyQuery($postId: Int) {
//       post(idType: DATABASE_ID, {id: $postId}) {
//         id
//         author {
//             node {
//               firstName
//               lastName
//             }
//         }
//         commentCount
//         date
//         featuredImage {
//           node {
//             sourceUrl
//           }
//         }
//         postId
//         title(format: RAW)
//         content
//       }
//   }    
// `;

export const POST_QUERY = gql` query MyQuery($postId: ID!) {
  post(idType: DATABASE_ID, id: $postId) {
    id
    author {
      node {
        firstName
        lastName
      }
    }
    commentCount
    date
    featuredImage {
      node {
        sourceUrl
      }
    }
    postId
    title(format: RAW)
    content
  }
}
`;