//import './App.css';
import {POST_QUERY} from './postQuery';
import { useQuery } from '@apollo/client';
import { Link } from 'react-router-dom'



function Post(props) {
    
    let postId = parseInt(props.match.params.id);
    
    
    const { loading, data } = useQuery(POST_QUERY, {
        variables: { postId },
      });

    if(loading){
        return 'loading';
    }
    console.log(data);
    let post = data.post;
    

    
  return (
          <div className="post-wrap container">
              <div className='row'>
                <div key={post.id} className='col-md-8'>
                <Link to={`/`}><button>back</button></Link>
                        <div    className='featureimg' 
                                style={{ backgroundImage:post.featuredImage ? `url(${post.featuredImage.node.sourceUrl})`: undefined }}>  
                        </div>
                        <h2  >{post.title}</h2>
                        <div  dangerouslySetInnerHTML={{__html : post.content}}></div>
                    </div>
              </div>
          </div>
    );
}

export default Post;
