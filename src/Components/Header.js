import React from 'react';
import { NavLink } from 'react-router-dom';
import S from './header.module.css';


const Header = () => {
    return (
        <div className={S.wrap}>
            <div className={S.navbar}>
                <ul>
                    <li><NavLink exact to='/' activeClassName={S.active}>Home</NavLink></li>
                    <li><NavLink exact to='/about' activeClassName={S.active}>About</NavLink></li>
                    <li><NavLink exact to='/blog' activeClassName={S.active}>Blog</NavLink></li>
                    <li><NavLink exact to='/shop' activeClassName={S.active}>Shop</NavLink></li>
                    
                </ul>
            </div>
        </div>
    );
}
export default Header;